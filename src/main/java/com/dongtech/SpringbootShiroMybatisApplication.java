package com.dongtech;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan    //扫描Servlet
@MapperScan("mapper")	//这里mapper是你的mybatis的mapper目录。
@SpringBootApplication
public class SpringbootShiroMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootShiroMybatisApplication.class, args);
        System.out.println("" +
        "-------"     +   "\n" +
        "|   _  \\"     +   "\n" +
        "|  |_|  |"+"dongtech已启动"     +   "\n" +
        "|      /"     +   "\n" +
        "-------"     +   "\n");
    }
}
